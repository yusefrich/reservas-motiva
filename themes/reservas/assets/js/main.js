$(document).ready(function () {


	$('.registration').each(function () {
		$(".form").validate();
		$('#integral').hide();
		// Swiper
		var swiper = new Swiper('.swiper', { simulateTouch: false, allowTouchMove: false })

		// Forms
		$(".form").each(function (index) {

			var form = $(this);
			// On form submition
			form.on("submit", function (event) {
			    if (index == 3) {

					let data = new FormData(),
						guarantor = $('[name="registration[guarantor]"]:checked').val()

					$("form.form :input").each(function () {
						console.log($(this)); // This is the jquery object of the input, do what you will
					});

					$('.form').each(function () {
						$(this).serializeArray().map(function (field) {
							data.append(field.name, field.value);
						})

						$('form.form :input[type="file"]').each(function () {
							$this = $(this);

							$.each($this[0].files, function(key, file) {
								data.append($this[0].name, file);
							});
						})
					})

					// Verify if guarantor is anyone else
					if (guarantor == 'other') {
						// If form is valid, then submit it
						if (form.valid()) {
							$('button[type="submit"]').prop('disabled', true)

							$.ajax({
								type: 'POST',
                                url: window.location+'/v1/registration',
								data: data,
								processData: false,
								contentType: false,
								enctype: 'multipart/form-data',
                                beforeSend: function () {
                                    //Aqui adicionas o loader
                                    $("button").text("Enviando, aguarde...");
                                },
                                success(data) {
									$('button[type="submit"]').prop('disabled', false)

									window.location.href = '/confirmacao'
								},
								error(e) {
									$('button[type="submit"]').prop('disabled', false)
								}
							})
						}
					} else {
						$('button[type="submit"]').prop('disabled', true)

						// If guarator is father or mother, submit it
						$.ajax( {
							type: 'POST',
                            url: window.location+'/v1/registration',
							cache: false,
							processData: false,
							contentType: false,
							enctype: 'multipart/form-data',
							data: data,
                            beforeSend: function () {
                                //Aqui adicionas o loader
                                $("button").text("Enviando, aguarde...");
							},
							xhr: function() {
								var xhr = $.ajaxSettings.xhr();
								xhr.upload.onprogress = function(e) {
									$('#post_progress').removeClass('d-none');
									$('#post_progress_slider').css('width', Math.floor(e.loaded / e.total *100) + '%');
									$('#post_progress_slider').html(Math.floor(e.loaded / e.total *100) + '%');

									if(Math.floor(e.loaded / e.total *100) == 100){
										$('#post_progress_slider').html('Redirecionando...');
									}
									console.log(Math.floor(e.loaded / e.total *100) + '%');
								};
								return xhr;
							},

							success(data) {
								$('button[type="submit"]').prop('disabled', false)

								window.location.href = '/confirmacao'
							},
							error(e) {
								$('button[type="submit"]').prop('disabled', false)
							}
						})
					}
				} else if (form.valid()) {
					swiper.slideTo(index + 1)
				}

				event.preventDefault()
			})
		});

		// On unit change
		$('select[name="registration[unit_id]"]').on('change', function (event) {
			$('#integral').hide();
			$('.edital').hide()

			let option = event.target.selectedOptions[0]
			$('.edital[data-region="' + option.dataset.region + '"]').show()
			var stages = JSON.parse(option.dataset.stages)
			var unit = { 'Ambiental': '1', 'Miramar': '2', 'Oriental': '3', 'Centro': '4', 'Jardim_Ambiental': '5' }
			var unit_id = event.target.value;
			let grade = $('select[name="registration[grade_id]"]')
			grade.html('')
			grade.append('<option value="">Selecione</option>')
			$('.radio-inline').hide()

			stages.forEach(function (stage) {
				$.request('onUnitsChange', {
					type: 'POST',
					data: {
						stage: stage
					},
					success(data) {
						// console.log(data)
						excludes_ambiental = [17]
						excludes_oriental = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17]
						turmas_bilingue = [18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]
						excludes_jardim_ambiental = [29]

						data.sort(function (a, b) {
							return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
						});
						data.forEach(option => {
							//   console.log("ID: "+option.id+" |  NOME: "+option.name)

                            console.log(option.id+" -->"+option.name);

							if (unit_id == unit.Ambiental && excludes_ambiental.indexOf(option.id) == '-1') {
								grade.append('<option value="' + option.id + '">' + option.name + '</option>')
							}

							if (unit_id == unit.Miramar && turmas_bilingue.indexOf(option.id) == '-1') {
								grade.append('<option value="' + option.id + '">' + option.name + '</option>')
							}

							if (unit_id == unit.Oriental && excludes_oriental.indexOf(option.id) == '-1') {
								grade.append('<option value="' + option.id + '">' + option.name + '</option>')
							}

							if (unit_id == unit.Jardim_Ambiental && excludes_jardim_ambiental.indexOf(option.id) == '-1') {
								grade.append('<option value="' + option.id + '">' + option.name + '</option>')
							}

							if (unit_id == unit.Centro && turmas_bilingue.indexOf(option.id) == '-1') {
								grade.append('<option value="' + option.id + '">' + option.name + '</option>')
							}
						})

					}
				})

			});
		})

		$('select[name="registration[grade_id]"]').on('change', function (event) {

			unit_selected = $('select[name="registration[unit_id]"]').val();
			var ensino_medio = ['15', '16', '17']
			if (unit_selected == 3) {
				var turmas = ['2', '13', '14', '18', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29']

				if (turmas.indexOf(event.target.value) != '-1') {
					$('#integral').show();
				} else {
					$('#integral').hide();
				}
			}

			if (ensino_medio.indexOf(event.target.value) != '-1') {
				$('#t_tarde').hide();
			} else {
				$('#t_tarde').show();
			}

			var inf_1_2_3 = ['1', '2', '3', '18']

			if (inf_1_2_3.indexOf(event.target.value) != '-1') {
				$(".bloco-agendamento").hide();
				$(".bloco-agendamento-info").show();
			}
			else {
				$(".bloco-agendamento-info").hide();
				$(".bloco-agendamento").show();
			}

		})

		// On guarantor change
		$('input[name="registration[guarantor]"]').on('change', function (event) {
			if (event.target.value == 'other') {
				$(".other").show();
			} else {
				$(".other").hide();
			}
		})
	});

    $("input[type='file']").on("change", function () {
        if(this.files[0].size > 1526000) {
            alert("Imagem maior que 1MB. Favor, anexar novamente.");
        }
        var ext = $(this).val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            alert('Arquivo nāo suportado. Opções disponíveis: JPG,JPEG,PNG');
        }
    });

	$('input[name="scheduling[attendance]"]').on('change', function (event) {
		$('#scheduling_date').val("");
		$(".agendamento-modo").show();
		if (event.target.value === "virtual") {
			$(".info-presencial").hide();
			$(".info-virtual").show();
		}
		if (event.target.value === "presencial") {
			$(".info-virtual").hide();
			$(".info-presencial").show();
		}
	});

	// Phone Masks
	$('.phone-mask').each(function () {
		new Cleave($(this), { phone: true, phoneRegionCode: 'br' })
	})

	// Cep masks
	$('.cep-mask').each(function () {
		new Cleave($(this), { delimiter: '-', blocks: [5, 3] })
	})

	// Cpf masks
	$('.cpf-mask').each(function () {
		new Cleave($(this), { delimiters: ['.', '.', '-'], blocks: [3, 3, 3, 2] })
	})

	// Date masks
	$('.date-mask').each(function () {
		new Cleave($(this), { date: true, datePattern: ['d', 'm', 'Y'] })
	})

	$('input[name="father[name]"]').blur(function () {

		$('.radio-inline').show()
	})


});
