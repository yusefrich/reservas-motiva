// Aluno

$("input[name='student[zipcode]']").blur(function(){
    // Remove tudo o que não é número para fazer a pesquisa
    var cep = this.value.replace(/[^0-9]/, "");

    if(cep.length != 8){
        return false;
    }
    var url = "https://viacep.com.br/ws/"+cep+"/json/";

    $.getJSON(url, function(dadosRetorno){
        try{
            // Preenche os campos de acordo com o retorno da pesquisa
            $("input[name='student[street]']").val(dadosRetorno.logradouro);
            $("input[name='student[neighbourhood]']").val(dadosRetorno.bairro);
            $("input[name='student[city]']").val(dadosRetorno.localidade);
        }catch(ex){}
    });
});


// pai

$("input[name='father[zipcode]']").blur(function(){
    // Remove tudo o que não é número para fazer a pesquisa
    var cep = this.value.replace(/[^0-9]/, "");

    if(cep.length != 8){
        return false;
    }
    var url = "https://viacep.com.br/ws/"+cep+"/json/";

    $.getJSON(url, function(dadosRetorno){
        try{
            // Preenche os campos de acordo com o retorno da pesquisa
            $("input[name='father[street]']").val(dadosRetorno.logradouro);
            $("input[name='father[neighbourhood]']").val(dadosRetorno.bairro);
            $("input[name='father[city]']").val(dadosRetorno.localidade);
        }catch(ex){}
    });
});


// mae

$("input[name='mother[zipcode]']").blur(function(){
    // Remove tudo o que não é número para fazer a pesquisa
    var cep = this.value.replace(/[^0-9]/, "");

    if(cep.length != 8){
        return false;
    }
    var url = "https://viacep.com.br/ws/"+cep+"/json/";

    $.getJSON(url, function(dadosRetorno){
        try{
            // Preenche os campos de acordo com o retorno da pesquisa
            $("input[name='mother[street]']").val(dadosRetorno.logradouro);
            $("input[name='mother[neighbourhood]']").val(dadosRetorno.bairro);
            $("input[name='mother[city]']").val(dadosRetorno.localidade);
        }catch(ex){}
    });
});

// responsavel finaceiro

$("input[name='guarantor[zipcode]']").blur(function(){
    // Remove tudo o que não é número para fazer a pesquisa
    var cep = this.value.replace(/[^0-9]/, "");

    if(cep.length != 8){
        return false;
    }
    var url = "https://viacep.com.br/ws/"+cep+"/json/";

    $.getJSON(url, function(dadosRetorno){
        try{
            // Preenche os campos de acordo com o retorno da pesquisa
            $("input[name='guarantor[street]']").val(dadosRetorno.logradouro);
            $("input[name='guarantor[neighbourhood]']").val(dadosRetorno.bairro);
            $("input[name='guarantor[city]']").val(dadosRetorno.localidade);
        }catch(ex){}
    });
});
