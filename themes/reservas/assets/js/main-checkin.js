$(document).ready(function(){
	$('.main-checkin').each(function(){
		let loading = false,
			services = false,
			data = {
				student: '',
				cpf: '',
				address: '',
				email: '',
				name: '',
				phone: '',
				region: '',
				services: [],
				shift: '',
				stage: '',
				unit_id: '',
				grade_id: ''
			}

		// Swiper
		let swiper = new Swiper('.swiper', {
			simulateTouch: false,
            allowTouchMove: false,
			on: {
				slideChange: function () {
					var $steps = $('.list-step'),
						swiper = this

					$steps.removeClass('active checked')
					$steps.each(function(index){
						if (index < swiper.activeIndex)
							$(this).addClass('checked')
						else if (index == swiper.activeIndex)
							$(this).addClass('active')
					})
				},
			},
		})

		function update() {
			services = false

			// Prevent repeating services data
			data.services = []

			// Updates data object
			$('.form').each(function() {
				$(this).serializeArray().map(function(field) {
					if (field.name == 'services[]')
						data.services.push({
							service: field.value
						})
					else
						data[field.name] = field.value
				})
			})

			// Toggle services
			$('.service').each(function() {
				let $service = $(this),
					grades = $service
						.data('grades').split(',')
						.map(grade => parseInt(grade))

				// Toggle services
				if (
					$service.data('region') == data.region &&
					grades.indexOf(data.grade_id) != -1
				) {
					services = true
					$service.show()
				} else {
					$service.hide()
				}
			})
		}

		// Forms
		$(this).find(".form")
			.each(function (index) {
				let $form = $(this)

				// Initialize validation
				$form.validate()

				// On form submition
				$form.on("submit", function (event) {
					update()

					if (index == 0 && data.student != '') {

						$.request('onValidate', {
							type: 'POST',
							data: data,
							success: response => {

								if(response != false)
									alert('Checkin já foi realizado para o(a) aluno(a) selecionado(a)');
								else
									swiper.slideTo(1)
							},

						})


					} else if (index == 1 && $form.valid()) {
						swiper.slideTo(2)

					} else if (
						index == 2 && data.unit_id != '' &&
						data.shift != '' &&
						!services ||
						index == 3
					) {
						if (!loading) {
							loading = true

							$.request('onSubmit', {
								type: 'POST',
								data: data,
								success: response => {
									swiper.slideTo(4)
									loading = false
								},
								error: () => loading = false
							})
						}

					} else if (index == 2 && data.unit_id != '' && data.shift != '') {
						swiper.slideTo(3)
					}

					event.preventDefault()
				})
			})

		// Back buttons
		$('.previous').on('click', event => {
			event.preventDefault()
			swiper.slidePrev()
		})

		// Select field
		$('.field-select').each(function(){
			let $select = $(this),
				$input = $select.find('input'),
				$label = $select.find('.selected'),
				$options = $select.find('li')

			$options.each(function(index) {
				$option = $(this)

				$option.on('click', event => {
					$input.val(this.dataset.value)
				})
			})

			this.reset = function() {
				$input.val('')
				$label.html($select.find('.span-icon').html())
			}
		})

		// Region change
		$('input[name="region"]').on('change', event => {
			let region = event.target.value,
				$unit = $('.unit'),
				$options = $unit.find('[data-region="' + region + '"]')

			$unit[0].reset()
			$unit.find('li').hide()

			$options.show()
			data.region = region

            $('.region').each(function() {
                if (data.region !== $(this).data('region')) {
                    $(this).hide();
                }else{
                    $(this).show();
                }
		    })
        })
		// On student change
		$('input[name="student"]').on('change', event => {
			let grade = JSON.parse(event.target.dataset.grade)
        //    console.log(event.target.dataset.grade);
			// Update grade data
			$('input[name="grade"]').val(grade.name)

			data.grade_id = grade.id
			data.stage = grade.stage
		})
	})

	// First form
	$('.cpf-form').each(() => {
		let $cpf = $(this).find('input[name="cpf"]')

		$(this).validate()

		$(this).on('submit', event => {
			if ($cpf.val().length < 14)
				event.preventDefault()
		})
	})
})
