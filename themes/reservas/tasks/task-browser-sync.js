var gulp = require('gulp');
var watch = require('gulp-watch');
var php = require('gulp-connect-php');
var browserSync = require('browser-sync').create();

gulp.task('server.watch', () => {
	watch('assets/scss/**/*.scss', () => gulp.start('scss'))
	watch('assets/js/**/*.js', () => gulp.start('scripts'))
});

gulp.task('browser-sync', ['server.watch'], () => {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});
});
