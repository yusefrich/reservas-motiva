var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');

gulp.task('scss.build', ['scss'])

gulp.task('scss', () => {
	return gulp.src(['assets/scss/**/*.scss'])
		.pipe(sass())
		.pipe( autoprefixer( 'last 20 version' ) )
		.pipe(concat('build.css'))
		.pipe(minifyCSS())
		.pipe(gulp.dest('dist'))
});
