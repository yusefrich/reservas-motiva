var gulp = require('gulp');
var concat = require('gulp-concat');
var minifyjs = require('gulp-js-minify');

gulp.task('scripts.build', ['scripts'])

gulp.task('scripts', () =>  {
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/jquery-validation/dist/jquery.validate.min.js',
		'node_modules/swiper/dist/js/swiper.js',
		'node_modules/cleave.js/dist/cleave.min.js',
		'node_modules/cleave.js/dist/addons/cleave-phone.br.js',
		'assets/js/main.js',
		'assets/js/main-checkin.js',
	])
		.pipe(concat('build.js'))
	//.pipe(minifyjs())
		.pipe(gulp.dest('dist'));
});
