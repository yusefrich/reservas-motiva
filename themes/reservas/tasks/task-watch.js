var gulp = require('gulp');
var watch = require('gulp-watch');

gulp.task('watch', () => {
    gulp.watch('assets/scss/**/*.scss', ['scss']);
    gulp.watch('assets/js/**/*.js', ['scripts']);
});
