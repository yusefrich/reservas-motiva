<?php namespace Motiva\Booking;

use Backend;
use System\Classes\PluginBase;

/**
 * Booking Plugin Information File
 */
class Plugin extends PluginBase
{
	/**
	 * Returns information about this plugin.
	 *
	 * @return array
	 */
	public function pluginDetails()
	{
		return [
			'name'        => 'Booking',
			'description' => 'No description provided yet...',
			'author'      => 'Motiva',
			'icon'        => 'icon-leaf'
		];
	}

	/**
	 * Register method, called when the plugin is first registered.
	 *
	 * @return void
	 */
	public function register()
	{

	}

	/**
	 * Boot method, called right before the request route.
	 *
	 * @return array
	 */
	public function boot()
	{

	}

	/**
	 * Registers any front-end components implemented in this plugin.
	 *
	 * @return array
	 */
	public function registerComponents()
	{
		return []; // Remove this line to activate

		return [
			'Motiva\Booking\Components\MyComponent' => 'myComponent',
		];
	}

	/**
	 * Registers any back-end permissions used by this plugin.
	 *
	 * @return array
	 */
	public function registerPermissions()
	{
		return []; // Remove this line to activate

		return [
			'motiva.booking.some_permission' => [
				'tab' => 'Booking',
				'label' => 'Some permission'
			],
		];
	}
}
