<?php namespace Motiva\Booking\Tests;

use PluginTestCase;
use Motiva\Booking\Models\Checkin;

class CheckinTest extends PluginTestCase
{
	public function testGetNextGrade()
	{
		// Ensino Infantil
		$this->assertEquals(Checkin::getNextGrade('Infantil', 'Infantil I')->name, 'Infantil II');
		$this->assertEquals(Checkin::getNextGrade('Infantil', 'Infantil II')->name, 'Infantil III');
		$this->assertEquals(Checkin::getNextGrade('Infantil', 'Infantil III')->name, 'Infantil IV');
		$this->assertEquals(Checkin::getNextGrade('Infantil', 'Infantil IV')->name, 'Infantil V');
		$this->assertEquals(Checkin::getNextGrade('Infantil', 'Infantil V')->name, '1º Ano');

		// Ensino Fundamental
		$this->assertEquals(Checkin::getNextGrade('Fundamental I', '1º Ano')->name, '2º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental I', '2º Ano')->name, '3º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental I', '3º Ano')->name, '4º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental I', '4º Ano')->name, '5º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental I', '5º Ano')->name, '6º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental II', '6º Ano')->name, '7º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental II', '7º Ano')->name, '8º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental II', '8º Ano')->name, '9º Ano');
		$this->assertEquals(Checkin::getNextGrade('Fundamental II', '9º Ano')->name, '1ª Série');

		// Ensino Médio
		$this->assertEquals(Checkin::getNextGrade('Ensino Médio', '1ª Série')->name, '2ª Série');
		$this->assertEquals(Checkin::getNextGrade('Ensino Médio', '2ª Série')->name, '3ª Série');
		$this->assertEquals(Checkin::getNextGrade('Ensino Médio', '3ª Série'), null);
	}
}
