<?php namespace Motiva\Booking\Models;

use Model;

/**
 * Grade Model
 */
class Grade extends Model
{
	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'motiva_booking_grades';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [];

	/**
	 * Automatically set created_at and updated_at fields.
	 *
	 * @var array
	 */
	public $timestamps = false;

	/**
	 * @var array Relations
	 */
	public $hasOne = [];
	public $hasMany = [];
	public $belongsTo = [];
	public $belongsToMany = [];
	public $morphTo = [];
	public $morphOne = [];
	public $morphMany = [];
	public $attachOne = [];
	public $attachMany = [];
}
