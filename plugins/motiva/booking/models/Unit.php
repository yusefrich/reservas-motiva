<?php namespace Motiva\Booking\Models;

use Model;

/**
 * unit Model
 */
class Unit extends Model
{
	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'motiva_booking_units';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [];

	/**
	 * Automatically set created_at and updated_at fields.
	 *
	 * @var array
	 */
	public $timestamps = false;

	public $jsonable = ['stages'];

	/**
	 * @var array Relations
	 */
	public $hasOne = [];
	public $hasMany = [];
	public $belongsTo = [];
	public $belongsToMany = [];
	public $morphTo = [];
	public $morphOne = [];
	public $morphMany = [];
	public $attachOne = [];
	public $attachMany = [];
}
