<?php namespace Motiva\Booking\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Model;
use Motiva\Booking\Models\Grade;

/**
 * Checkin Model
 */
class Checkin extends Model
{
	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'motiva_booking_checkins';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [
		'name', 'student', 'cpf', 'address', 'email', 'phone',
		'services', 'stage', 'shift', 'grade_id', 'unit_id'
	];

	/**
	 * @var array Jsonable fields
	 */
	protected $jsonable = ['services'];

	/**
	 * @var array Relations
	 */
	public $hasOne = [];
	public $hasMany = [];
	public $belongsTo = [
		'grade' => 'Motiva\Booking\Models\Grade',
		'unit' => 'Motiva\Booking\Models\Unit',
	];
	public $belongsToMany = [];
	public $morphTo = [];
	public $morphOne = [];
	public $morphMany = [];
	public $attachOne = [];
	public $attachMany = [];

	public function getGradeOptions()
	{
		$grades = Grade::where('stage', $this->stage)->get();

		return $grades->mapWithKeys(function($grade) {
			return [$grade->id => $grade->name];
		});
	}

	public static function getNextGrade($stage, $grade) {


		$next = ['stage' => '', 'grade' => ''];

		switch ($stage) {
		case 'Infantil':
			$next['stage'] = 'infantil';

			switch($grade) {
			case 'Infantil I':
				$next['grade'] = 'Infantil II';
				break;
			case 'Infantil II':
				$next['grade'] = 'Infantil III';
				break;
			case 'Infantil III':
				$next['grade'] = 'Infantil IV';
				break;
			case 'Infantil IV':
				$next['grade'] = 'Infantil V';
				break;
			case 'Infantil V':
				$next['stage'] = 'fundamental';
				$next['grade'] = 'Fundamental - 1º Ano';
				break;
			}
			break;
			case 'Fundamental I':
				$next['stage'] = 'fundamental';

			case 'Fundamental II':
				$next['stage'] = 'fundamental';

			switch ($grade) {
			case '1º Ano':
				$next['grade'] = 'Fundamental - 2º Ano';
				break;
			case '2º Ano':
				$next['grade'] = 'Fundamental - 3º Ano';
				break;
			case '3º Ano':
				$next['grade'] = 'Fundamental - 4º Ano';
				break;
			case '4º Ano':
				$next['grade'] = 'Fundamental - 5º Ano';
				break;
			case '5º Ano':
				$next['grade'] = 'Fundamental - 6º Ano';
				break;
			case '6º Ano':
				$next['grade'] = 'Fundamental - 7º Ano';
				break;
			case '7º Ano':
				$next['grade'] = 'Fundamental - 8º Ano';
				break;
			case '8º Ano':
				$next['grade'] = 'Fundamental - 9º Ano';
				break;
			case '9º Ano':
				$next['stage'] = 'medio';
				$next['grade'] = 'Ensino Médio - 1ª Série';
				break;
			}

			break;
		case 'Ensino Médio':
			$next['stage'] = 'medio';

			switch ($grade) {
			case '1ª Série':
				$next['grade'] = 'Ensino Médio - 2ª Série';
				break;
			case '2ª Série':
				$next['grade'] = 'Ensino Médio - 3ª Série';
				break;
			}

			break;
		}

		return Grade::where([
			'stage' => $next['stage'],
			'name' => $next['grade']
		])
		->first();

	}

    public static function getStudent($cpf = null)
    {
        if(!$cpf)
            return false;
        try
        {
            $data = DB::connection("sqlsrv")->table('qualitare_checkin', '*')->where('CPF_RESP', $cpf)->get();

        } catch (\Exception $e)
        {
            Log::error($e->getMessage());
            return false;
        }
        return $data;
    }
}
