<?php namespace Motiva\Booking\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Registration Model
 */
class Registration extends Model
{
	use Validation;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'motiva_booking_registrations';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [
		'origin', 'stage', 'grade_id', 'shift', 'shift_full', 'unit_id'
	];

	/**
	 * @var array Validation fields
	 */
	public $rules = [
		'origin' => 'required',
	//	'stage' => 'required',
		'grade' => 'required'
	];

	/**
	 * @var array Relations
	 */
	public $hasOne = [];
	public $hasMany = [];
	public $belongsTo = [
		'unit' => 'Motiva\Booking\Models\Unit',
		'grade' => 'Motiva\Booking\Models\Grade',
		'student' => 'Motiva\Booking\Models\Person',
		'father' => 'Motiva\Booking\Models\Person',
		'mother' => 'Motiva\Booking\Models\Person',
		'guarantor' => 'Motiva\Booking\Models\Person'
	];
	public $belongsToMany = [];
	public $morphTo = [];
	public $morphOne = [];
	public $morphMany = [];
	public $attachOne = [];
	public $attachMany = [];

	public function getGradeOptions()
	{
		$grades = Grade::where('stage', $this->stage)->get();

		return $grades->mapWithKeys(function($grade) {
			return [$grade->id => $grade->name];
		});
	}
}
