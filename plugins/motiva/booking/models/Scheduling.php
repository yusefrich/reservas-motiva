<?php namespace Motiva\Booking\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use Illuminate\Support\Facades\DB;
/**
 * Registration Model
 */
class Scheduling extends Model
{
	use Validation;

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'motiva_booking_registration_scheduling';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [
		'date', 'attendance', 'registration_id',
	];

    /**
	 * @var array Validation fields
	 */
	public $rules = [
		'date' => 'required',
		'registration_id' => 'required',
		'attendance' => 'required'
	];

	/**
	 * @var array Relations
	 */
	public $hasOne = [];
	public $hasMany = [];
	public $belongsTo = [
		'registration' => ['Motiva\Booking\Models\Registration'],
	];
    /**
     * @var mixed|string
     */

    public function getRegistrationIdAttribute()
    {
        try {
            $student = DB::table('motiva_booking_people')->select('name')
                ->leftJoin('motiva_booking_registrations', 'motiva_booking_people.id', '=', 'motiva_booking_registrations.student_id')
                ->where('motiva_booking_registrations.id', $this->attributes['registration_id'])->get();

            return $student[0]->name;
        } catch (\Exception $e)
        {
            return "NOME NÃO LOCALIZADO";
        }

	}

}
