<?php namespace Motiva\Booking\Models;

use Illuminate\Http\Request;
use Model;
use October\Rain\Database\Traits\Validation;
use Illuminate\Support\Facades\Storage;

/**
 * Person Model
 */
class Person extends Model
{
	use Validation;

	public $implement = ['RainLab.Location.Behaviors.LocationModel'];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'motiva_booking_people';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];


    /**
     * @var array Fillable fields
     */
	protected $fillable = [
		'name', 'birthday', 'citzenship', 'citzenship_state', 'rg', 'cpf',
		'phone_1', 'phone_2', 'phone_3', 'zipcode', 'street',
		'number', 'neighbourhood', 'city', 'state_code', 'civil',
		'occupation', 'email', 'work', 'gender', 'kinship',
        'photo', 'cpf_file', 'rg_file', 'birth_registration', 'report_card'
    ];

	/**
	 * @var array Validation fields
	 */
	public $rules = [
		'name' => 'required',
		'birthday' => 'required',
		'gender' => 'required',
		'street' => 'required',
		'number' => 'required',
		'neighbourhood' => 'required',
		'city' => 'required',
		'citzenship' => 'required',
		'citzenship_state' => 'required',
        'phone_1' => 'required',
		'photo' => 'max:191',
        'cpf_file' => 'max:191',
        'rg_file' => 'max:191',
        'birth_registration' => 'max:191',
        'report_card' => 'max:191',
	];

	/**
	 * Automatically set created_at and updated_at fields.
	 *
	 * @var array
	 */
	public $timestamps = false;

	/**
	 * @var array Relations
	 */
	public $hasOne = [
		'registration' => 'Motiva\Booking\Models\Registration'
	];
	public $hasMany = [];
	public $belongsTo = [
		'state' => 'RainLab\Location\Models\State'
	];
	public $belongsToMany = [];
	public $morphTo = [];
	public $morphOne = [];
	public $morphMany = [];
	public $attachMany = [];

	public  function  getPhotoAttribute($value)
    {
        if(!$value)
            return "";

        return url(Storage::url($value));
    }

    public  function  getCpfFileAttribute($value)
    {
        if(!$value)
            return "";

        return url(Storage::url($value));
    }

    public  function  getRgFileAttribute($value)
    {
        if(!$value)
            return "";

        return url(Storage::url($value));
    }

    public  function  getBirthRegistrationAttribute($value)
    {
        if(!$value)
            return "";

        return url(Storage::url($value));
    }

    public  function  getReportCardAttribute($value)
    {
        if(!$value)
            return "";

        return url(Storage::url($value));
    }
}
