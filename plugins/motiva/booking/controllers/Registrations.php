<?php namespace Motiva\Booking\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Http\Request;
use Motiva\Booking\Models\Registration;
use Motiva\Booking\Models\Scheduling;
use Motiva\Booking\Models\Person;
use Illuminate\Support\Facades\Storage;
use Mail;

/**
 * Registrations Back-end Controller
 */
class Registrations extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Motiva.Booking', 'booking', 'registrations');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['student']['birthday'] = $this->formatDate($data['student']['birthday']);
        $data['father']['birthday'] = $this->formatDate($data['father']['birthday']);
        $data['mother']['birthday'] = $this->formatDate($data['mother']['birthday']);

        $files = $request->allFiles();

        //aluno
        $data['student']['photo'] = Storage::putFile('uploads/public/registrations/foto/'.date('Y'), $files['student']['photo']);
        $data['student']['birth_registration'] = Storage::putFile('uploads/public/registrations/registro/'.date('Y'), $files['student']['birth_registration']);
        $data['student']['cpf_file'] = Storage::putFile('uploads/public/registrations/cpf/'.date('Y'), $files['student']['cpf_file']);
        $data['student']['report_card'] = Storage::putFile('uploads/public/registrations/boletim/'.date('Y'), $files['student']['report_card']);
        $student = Person::create($data['student']);

        //pai
        $data['father']['rg_file'] = Storage::putFile('uploads/public/registrations/rg/'.date('Y'), $files['father']['rg_file']);
        $data['father']['cpf_file'] = Storage::putFile('uploads/public/registrations/cpf/'.date('Y'), $files['father']['cpf_file']);
        $father = Person::create($data['father']);

        //mae
        $data['mother']['rg_file'] = Storage::putFile('uploads/public/registrations/rg/'.date('Y'), $files['mother']['rg_file']);
        $data['mother']['cpf_file'] = Storage::putFile('uploads/public/registrations/cpf/'.date('Y'), $files['mother']['cpf_file']);
        $mother = Person::create($data['mother']);

        $registration = Registration::create($data['registration']);

        // Attach parents & student
        $registration->student()->associate($student);
        $registration->father()->associate($father);
        $registration->mother()->associate($mother);

        // Attach guarantor
        if ($data['registration']['guarantor'] == 'other')
        {
            $data['guarantor']['birthday'] = $this->formatDate($data['student']['birthday']);
            $data['guarantor']['rg_file'] = Storage::putFile('uploads/public/registrations/rg/'.date('Y'), $files['guarantor']['rg_file']);
            $data['guarantor']['cpf_file'] = Storage::putFile('uploads/public/registrations/cpf/'.date('Y'), $files['guarantor']['cpf_file']);
            $guarantor = Person::create($data['guarantor']);
            $registration->guarantor()->associate($guarantor);
        }
        else if ($data['registration']['guarantor'] == 'father')
        {
            $registration->guarantor()->associate($father);
        }
        else if ($data['registration']['guarantor'] == 'mother')
        {
            $registration->guarantor()->associate($mother);
        }

        // Update registration
        $registration->save();
        $date = new \DateTime($registration->created_at);

        // Scheduling
        if(!empty($data['scheduling']['date'])){
            $data['scheduling']['registration_id'] = $registration->id;
            $scheduling = Scheduling::create($data['scheduling']);
            $scheduling->save();
        }

       // Send email
        	Mail::send('motiva.booking::mail.message', [
                'timestamp' => $date->getTimestamp(),
                'id' => $registration->id,
                'student' => $student->name
            ], function($message) use ($mother, $father) {
                $message->to($father->email, $father->name);
                $message->cc($mother->email, $mother->name);
                $message->bcc('projetos@qualitare.com.br', 'Projetos Qualitare');
                $message->subject('Obrigado!');
            });

            return $registration;
    }

    public function formatDate($date)
    {
        if(!$date)
           return false;

        return implode("-", array_reverse(explode("/", $date)));
    }

}
