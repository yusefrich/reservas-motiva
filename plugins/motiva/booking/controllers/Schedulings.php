<?php namespace Motiva\Booking\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Http\Request;
use Motiva\Booking\Models\Scheduling;

/**
 * Scheduling Back-end Controller
 */
class Schedulings extends Controller
{
	public $implement = [
		'Backend.Behaviors.ListController',
	];

	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('Motiva.Booking', 'booking', 'scheduling');
	}
}
