<?php namespace Motiva\Booking\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Checkins Back-end Controller
 */
use GuzzleHttp\Client;

class Checkins extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Motiva.Booking', 'booking', 'checkins');
    }
}
