<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUnitsStageField extends Migration
{
	public function up()
	{
        if (!Schema::hasColumn('motiva_booking_units', 'stages')){
                Schema::table('motiva_booking_units', function(Blueprint $table) {
                    $table->string('stages')->nullable();
                });
        }
	}

	public function down()
	{
		Schema::dropIfExists('motiva_booking_units');
	}
}
