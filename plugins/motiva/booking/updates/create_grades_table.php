<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGradesTable extends Migration
{
	public function up()
	{
        if (!Schema::hasTable('motiva_booking_grades')) {
                Schema::create('motiva_booking_grades', function(Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('name');
                    $table->string('stage');
                });
        }
	}

	public function down()
	{
		Schema::dropIfExists('motiva_booking_grades');
	}
}
