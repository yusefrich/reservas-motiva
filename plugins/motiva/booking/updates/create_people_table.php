<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePeopleTable extends Migration
{
	public function up()
	{
        if (!Schema::hasTable('motiva_booking_people')) {
            Schema::create('motiva_booking_people', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->date('birthday');
                $table->string('gender')->nullable();
                $table->string('citzenship');
                $table->string('citzenship_state');
                $table->string('kinship')->nullable();
                $table->string('rg')->nullable();
                $table->string('cpf')->nullable();
                $table->string('email')->nullable();
                $table->string('occupation')->nullable();
                $table->string('work')->nullable();
                $table->string('civil')->nullable();
                $table->string('zipcode');
                $table->string('street');
                $table->string('number');
                $table->string('neighbourhood');
                $table->string('city');
                $table->string('phone_1');
                $table->string('phone_2')->nullable();
                $table->string('phone_3')->nullable();
                $table->integer('state_id')->unsigned()->nullable()->index();
                $table->integer('country_id')->unsigned()->nullable()->index();
            });
        }
	}

	public function down()
	{
		Schema::dropIfExists('motiva_booking_people');
	}
}
