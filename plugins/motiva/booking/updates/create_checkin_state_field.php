<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCheckinsStateField extends Migration
{
	public function up()
	{
        if (!Schema::hasColumn('motiva_booking_checkins', 'state')){{
                Schema::table('motiva_booking_checkins', function(Blueprint $table) {
                $table->string('state');
                });
            }
        }
	}

	public function down()
	{
		Schema::table('motiva_booking_checkins', function(Blueprint $table) {
			$table->dropColumn('state');
		});
	}
}
