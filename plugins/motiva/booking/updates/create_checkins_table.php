<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCheckinsTable extends Migration
{
	public function up()
	{
        if (!Schema::hasTable('motiva_booking_checkins')) {
            Schema::create('motiva_booking_checkins', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('student');
                $table->string('cpf');
                $table->string('address');
                $table->string('email');
                $table->string('name');
                $table->string('phone');
                $table->text('services');
                $table->string('shift');
                $table->string('stage');
                $table->integer('grade_id');
                $table->integer('unit_id');
                $table->timestamps();
            });
        }
	}

	public function down()
	{
		Schema::dropIfExists('motiva_booking_checkins');
	}
}
