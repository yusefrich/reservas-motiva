<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRegistrationsTable extends Migration
{
	public function up()
	{
        if (!Schema::hasTable('motiva_booking_registrations')) {
            Schema::create('motiva_booking_registrations', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('origin');
                $table->string('stage')->nullable();
                $table->string('shift');
                $table->integer('shift_full')->default(0);
                $table->integer('state')->default(0);
                $table->string('grade_id')->nullable();
                $table->integer('unit_id')->nullable();
                $table->integer('student_id')->nullable();
                $table->integer('father_id')->nullable();
                $table->integer('mother_id')->nullable();
                $table->integer('guarantor_id')->nullable();
                $table->timestamps();
            });
        }
	}

	public function down()
	{
		Schema::dropIfExists('motiva_booking_registrations');
	}
}
