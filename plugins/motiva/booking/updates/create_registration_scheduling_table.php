<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRegistrationSchedulingTable extends Migration
{
	public function up()
	{
        if (!Schema::hasTable('motiva_booking_registration_scheduling')) {
            Schema::create('motiva_booking_registration_scheduling', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->dateTime('date');
                $table->string('attendance');
                $table->integer('registration_id')->unsigned()->index();
                $table->timestamps();
            });
        }
	}

	public function down()
	{
		Schema::dropIfExists('motiva_booking_people_scheduling');
	}
}
