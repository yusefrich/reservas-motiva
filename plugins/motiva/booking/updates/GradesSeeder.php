<?php

namespace Motiva\Booking\Updates;

use October\Rain\Database\Updates\Seeder;
use Motiva\Booking\Models\Grade;

class GradesSeeder extends Seeder {

	public function run() {
		// Ensino Infantil
		Grade::create(['name' => 'Infantil I', 'stage' => 'infantil']);
		Grade::create(['name' => 'Infantil II', 'stage' => 'infantil']);
		Grade::create(['name' => 'Infantil III', 'stage' => 'infantil']);
		Grade::create(['name' => 'Infantil IV', 'stage' => 'infantil']);
		Grade::create(['name' => 'Infantil V', 'stage' => 'infantil']);

		// Ensino Fundamental
		Grade::create(['name' => '1º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '2º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '3º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '4º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '5º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '6º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '7º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '8º Ano', 'stage' => 'fundamental']);
		Grade::create(['name' => '9º Ano', 'stage' => 'fundamental']);

		// Ensino Médio
		Grade::create(['name' => '1ª Série', 'stage' => 'medio']);
		Grade::create(['name' => '2ª Série', 'stage' => 'medio']);
		Grade::create(['name' => '3ª Série', 'stage' => 'medio']);
	}
}
