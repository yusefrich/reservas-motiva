<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUnitsTable extends Migration
{
	public function up()
	{
        if (!Schema::hasTable('motiva_booking_units')) {
            Schema::create('motiva_booking_units', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->string('region');
            });
        }
	}

	public function down()
	{
		Schema::dropIfExists('motiva_booking_units');
	}
}
