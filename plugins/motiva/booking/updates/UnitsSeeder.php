<?php

namespace Motiva\Booking\Updates;

use October\Rain\Database\Updates\Seeder;
use Motiva\Booking\Models\Unit;

class UnitsSeeder extends Seeder {

	public function run() {
		// João Pessoa
		Unit::create(['name' => 'Ambiental', 'region' => 'jp']);
		Unit::create(['name' => 'Miramar', 'region' => 'jp']);
		Unit::create(['name' => 'Oriental', 'region' => 'jp']);

		// Campina Grande
		Unit::create(['name' => 'Centro', 'region' => 'cg']);
		Unit::create(['name' => 'Jardim Ambiental', 'region' => 'cg']);
	}
}
