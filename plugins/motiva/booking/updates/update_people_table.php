<?php namespace Motiva\Booking\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePeopleTable extends Migration
{
	public function up()
	{
            Schema::table('motiva_booking_people', function (Blueprint $table) {
                $table->string('photo')->nullable();
                $table->string('cpf_file')->nullable();
                $table->string('rg_file')->nullable();
                $table->string('birth_registration')->nullable();
                $table->string('report_card')->nullable();
            });
	}

	public function down()
	{
        Schema::table('motiva_booking_people', function (Blueprint $table) {
            //
        });
	}
}
